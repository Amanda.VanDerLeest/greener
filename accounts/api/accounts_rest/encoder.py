from common.json import ModelEncoder
from .models import User
# rating


class UserEncoder(ModelEncoder):
    model = User
    properties = ["id", "email", "username", "city", "state", "zip", "is_staff", "is_superuser"]


# class RatingEncoder(ModelEncoder):
#     model = Rating
#     properties = ["seller", "buyer", "number"]
#     encoders = {"seller": UserEncoder()}
