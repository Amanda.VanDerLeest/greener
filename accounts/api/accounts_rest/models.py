from django.db import models
from django.contrib.auth.models import AbstractUser
# # from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
class User(AbstractUser):
    # phone_number = PhoneNumberField (unique = True)
    latitude = models.DecimalField(
        blank=True, decimal_places=16, max_digits=22, null=True
    )
    longitude = models.DecimalField(
        blank=True, decimal_places=16, max_digits=22, null=True
    )